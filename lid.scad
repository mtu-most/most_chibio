$fn=$preview? 16:64;

lidDia = 32;
lidH = 12;
o2ProbeDia = 12;
phProbeDia = 12;

bigTubeInDia = 2.5;
bigTubeOutDia = 4.5;
lilTubeInDia = 1.5875; // 1/16"
lilTubeOutDia = 4.7625; // 3/16"

vialNeckInDia = 22;
vialNeckOutDia = 25;
vialBodyInDia = 30;
vialBodyOutDia = 35;
vialTotalH = 95;
vialNeckH = 10;
vialBodyH = vialTotalH-vialNeckH;
vialBodySmoothRad = 3;

// Threads have 2mm thickness
// Thread spacing 1.2mm
// Single intersection point

module nipples() {
    cube();
}

module vial() {
    cylinder();
} 

module holes() {
    union(){
        translate([0,0,lidH/2]){
            translate([7,-3,0])cylinder(lidH+(lidH*0.125),d=o2ProbeDia);
            translate([-7,-3,0])cylinder(lidH+(lidH*0.125),d=phProbeDia);
            translate([0,10,0])cylinder(lidH+(lidH*0.125),d=bigTubeInDia);
            translate([-5,7,0])cylinder(lidH+(lidH*0.125),d=bigTubeInDia);
            translate([5,7,0])cylinder(lidH+(lidH*0.125),d=bigTubeInDia);
            translate([0,4,0])cylinder(lidH+(lidH*0.125),d=bigTubeInDia);
            translate([3,-12,0])cylinder(lidH+(lidH*0.125),d=bigTubeInDia);
            translate([-3,-12,0])cylinder(lidH+(lidH*0.125),d=bigTubeInDia);
        }
    }
}
/*
difference(){
    cylinder(lidH,d=lidDia);
    holes();
    translate([0,0,-2])cylinder(lidH,d=lidDia*.9);
}*/

nipples();
