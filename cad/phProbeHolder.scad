$fn = 32;

// All units in mm
postC2C=32;
postSuppWidth=6;
postSuppHeight=2.5;
postSuppDia=3;
dodgeLidSuppArms=20;    // This one...
dodgeLidSuppArmsRem=25;
lipThickness=4;
lipHoleAlignment=16;    // ...and this one to change the alignment of where the hole to the probe entry will be.
holderCylinderHeight=50;
holderCylinderID=4;
holderCylinderOD=8;
holderCylinderConeHeight=8;
bulbID=14;
bulbOD=18;
smidge=0.5;

module wideLegs() {
  union() {
      cube([postSuppWidth, postC2C+postSuppDia, postSuppHeight]);
      cube([postC2C+postSuppDia, postSuppWidth, postSuppHeight]);
    }
}

module lip() {
  difference() {
    translate([0, 0, lipThickness]) cube([dodgeLidSuppArms, dodgeLidSuppArms, lipThickness]);
    translate([0, 0, lipThickness]) cube([postSuppWidth, postSuppWidth, lipThickness]);
    translate([lipHoleAlignment, lipHoleAlignment, lipThickness]) cylinder(h=lipThickness, d=holderCylinderID, center=false);
  }
}

module postHoles() {
  translate([postSuppWidth/2, postSuppWidth/2, postSuppHeight]) cylinder(h=postSuppHeight, d=postSuppDia, center=false);
  translate([postSuppWidth/2, postC2C, 0]) cylinder(h=postSuppHeight, d=postSuppDia, center=false);
  translate([postC2C, postSuppWidth/2, 0]) cylinder(h=postSuppHeight, d=postSuppDia, center=false);
}

module stepUp() {
  translate([0,0,postSuppHeight]) cube([postSuppWidth, dodgeLidSuppArms, postSuppHeight]);
  translate([0,0,postSuppHeight]) cube([dodgeLidSuppArms, postSuppWidth, postSuppHeight]);
}

module underStep() {
  translate([0,0,postSuppHeight/2]) rotate([0,0,-45]) cube([postSuppWidth, dodgeLidSuppArmsRem, postSuppHeight], center=true);
}

module bulb() {
  translate([0, 0, holderCylinderHeight+bulbID/2-smidge]) difference() {
    sphere(d=bulbOD);
    sphere(d=bulbID);
    translate([0, 0, bulbOD/4]) cube([bulbOD, bulbOD, bulbOD/2], center=true);
  }
}

module holderCylinder() {
  difference() {  
    union() {
      //cylinder(h=holderCylinderConeHeight, d2=holderCylinderOD, d1=holderCylinderOD+2, center=false);
      bulb();
      cylinder(h=holderCylinderHeight, d=holderCylinderOD, center=false);
    }
    cylinder(h=holderCylinderHeight, d=holderCylinderID, center=false);  
  }
}

difference() {
  union() {
    //wideLegs();
    //stepUp();
    //lip();
    //translate([lipHoleAlignment, lipHoleAlignment, lipThickness*2])holderCylinder(); 
    holderCylinder();
  }
  //underStep();
  //postHoles();
}
